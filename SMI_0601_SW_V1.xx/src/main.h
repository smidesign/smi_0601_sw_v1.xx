/*
 * main.h
 *
 * Created: 5/12/2020 03:54:54 PM
 *  Author: mdev_soterix
 */ 


#ifndef MAIN_H_
#define MAIN_H_

#include <asf.h>
/* */
#define FIRMWARE_VERSION	0x0100		//First 2 digit = version, last 2 digits revision xx.xx



/* All device configurations here */


/* Functions */
//uint16_t shunt_kalman_filter(uint16_t adc_value);
uint16_t load_kalman_filter(uint16_t adc_value);


#endif /* MAIN_H_ */