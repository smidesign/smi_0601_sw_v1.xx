/*
 * push_button.c
 *
 * Created: 5/29/2020 06:31:14 PM
 *  Author: mdev_soterix
 */ 

#include "push_button.h"

/**
 * \brief Initialize the Push button
 * \param pb structure
 */
void init_pb(push_button pb){
	ioport_set_pin_dir(pb.pb_pin, IOPORT_DIR_INPUT);
	if(pb.pb_logic_level == PB_ACTIVE_HIGH){
		ioport_set_pin_mode(pb.pb_pin, IOPORT_MODE_PULLDOWN);
	}
	else{// if(pb.pb_logic_level == PB_ACTIVE_LOW){
		ioport_set_pin_mode(pb.pb_pin, IOPORT_MODE_PULLDOWN);
	}
}

/**
 * \brief Returns pb state
 * \param pb structure
 */
enum _pb_state is_pb_pressed(push_button pb){
	if(pb.pb_logic_level == PB_ACTIVE_HIGH){
		return ioport_get_pin_level(pb.pb_pin);
	}
	else {//if(pb.pb_logic_level == PB_ACTIVE_LOW){
		return !ioport_get_pin_level(pb.pb_pin);
	}
	
};