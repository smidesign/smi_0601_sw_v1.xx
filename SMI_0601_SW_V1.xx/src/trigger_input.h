/*
 * trigger_input.h
 *
 * Created: 5/12/2020 05:03:03 PM
 *  Author: mdev_soterix
 */ 


#ifndef TRIGGER_INPUT_H_
#define TRIGGER_INPUT_H_

#include <asf.h>

/* Defines and enums */
enum logic_level {
	TRIGGER_ACTIVE_HIGH,
	TRIGGER_ACTIVE_LOW
};

enum trigger_status{
	NOT_TRIGGERED = false,
	TRIGGERED = true
};

/* Structure and variables */
typedef struct trigger_in{
	unsigned int trigger_in_pin;
	enum logic_level trigger_in_logic_level;
}trigger_in;

/* Functions */
void init_trigger(trigger_in trigger);
enum trigger_status check_trigger(trigger_in trigger);



#endif /* TRIGGER_INPUT_H_ */