/*
 * push_button.h
 *
 * Created: 5/29/2020 06:31:00 PM
 *  Author: mdev_soterix
 */ 


#ifndef PUSH_BUTTON_H_
#define PUSH_BUTTON_H_

#include <asf.h>

enum _pb_logic_level{
	PB_ACTIVE_HIGH,
	PB_ACTIVE_LOW
};

enum _pb_state{
	PB_PRESSED = true,
	PB_NOT_PRESSED = false
	};
	
typedef struct push_button{
	uint32_t pb_pin;
	enum _pb_logic_level pb_logic_level;

}push_button;

//Functions
void init_pb(push_button pb);
enum _pb_state is_pb_pressed(push_button pb);

#endif /* PUSH_BUTTON_H_ */