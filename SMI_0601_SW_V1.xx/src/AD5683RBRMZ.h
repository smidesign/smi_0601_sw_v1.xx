/*
 * AD5683RBRMZ.h
 *
 * Created: 1/28/2016 12:49:22 PM
 *  Author: Mithlesh
 */ 

/*
 * AD5683RBRMZ
 * 16 BIT DAC
 * SPI
 */

#ifndef AD5683RBRMZ_H_
#define AD5683RBRMZ_H_

#include <asf.h>

// SPI PARAMETERS
#define AD5683_SPI_MASTER_BASE		SPI
#define AD5683_SPI_CHIP_SEL			0
#define AD5683_SPI_MODE				SPI_MODE_0
#define AD5683_SPI_CLOCK_SPEED		5000000

#define MAX_DAC_VALUE				0xFFFE
#define MIN_DAC_VALUE				0x0000

// FUNCTIONS
void init_AD5683(void);
void set_dac_value(uint16_t value);

#endif /* AD5683RBRMZ_H_ */