/*
 * eeprom.h
 *
 * Created: 5/15/2018 10:59:58 AM
 *  Author: Mithlesh
 */ 


#ifndef EEPROM_H_
#define EEPROM_H_

/* ********** HARDWARE ********** */
/*
 * AT25M02
 * SPI Serial EEPROM 2-Mbit (262,144 x 8)
 */

// PIN DEFINITIONS
// There pins are not part of SPI
#define AT25M02_N_WP_PIN			PIO_PA6_IDX
#define AT25M02_N_HOLD_PIN			PIO_PA4_IDX

// SPI PARAMETERS
#define AT25M02_SPI_MASTER_BASE			SPI
#define AT25M02_SPI_CHIP_SEL			3
#define AT25M02_SPI_MODE				SPI_MODE_0
#define AT25M02_SPI_CLOCK_SPEED			5000000	//Hz

// AT25M02 INSTRUCTION SET
#define AT25M02_WRSR			0x01	//Write status register (SR)
#define AT25M02_WRITE			0x02	//Write to memory array
#define AT25M02_READ			0x03	//Read from memory array
#define AT25M02_WRDI			0x04	//Reset write enable latch (WEL)
#define AT25M02_RDSR			0x05	//Read status register (SR)
#define AT25M02_WREN			0x06	//Set write enable latch (WEL)
#define AT25M02_LPWP			0x08	//Low power write poll

// STATUS REGISTER (SR) BIT DEFINITIONS
#define AT25M02_SR_WPEN_0		0x00	//Write-Protect-Enable disable
#define AT25M02_SR_WPEN_1		0x80	//Write-protect-Enable enable
#define AT25M02_SR_BP_0			0x00	//Block write protect level 0 (No protection)
#define AT25M02_SR_BP_1			0x04	//Block write protect level 1 (25% protection)
#define AT25M02_SR_BP_3			0x08	//Block write protect level 2 (50% protection)
#define AT25M02_SR_BP_4			0x0C	//Block write protect level 3 (100% protection)
//#define AT25M02_SR_WEL_0		0x00	//Write-Enable-Latch disable
//#define AT25M02_SR_WEL_1		0x02	//Write-Enable-Latch enable

#define AT25M02_SR_RDY			0x00	//Device is ready for a new sequence
#define AT25M02_SR_BSY			0x01	//Device is busy with an internal operation
#define AT25M02_SR_BSY_MSK		0x01

#define AT25M02_LPWP_RDY		0x00	//Low Power Write Poll return: Ready
#define AT25M02_LPWP_BSY		0xFF	//Low Power Write Poll return: Busy

//Error list
//MACROS
#define ENABLE_EEPROM_WP ioport_set_pin_level(AT25M02_N_WP_PIN, IOPORT_PIN_LEVEL_LOW);
#define DISABLE_EEPROM_WP ioport_set_pin_level(AT25M02_N_WP_PIN, IOPORT_PIN_LEVEL_HIGH);
#define ENABLE_EEPROM_HOLD ioport_set_pin_level(AT25M02_N_HOLD_PIN, IOPORT_PIN_LEVEL_LOW);
#define DISABLE_EEPROM_HOLD ioport_set_pin_level(AT25M02_N_HOLD_PIN, IOPORT_PIN_LEVEL_HIGH);

/* Functions */
void init_AT25M02(void);

void AT25Mxx_write_enable(void);
void AT25Mxx_write_disable(void);
uint8_t AT25Mxx_read_SR(void);
void AT25Mxx_write_SR(uint8_t);
uint32_t AT25Mxx_busy_wait(void);
uint32_t AT25Mxx_LPWP_wait(void);

uint32_t AT25Mxx_write_data(uint8_t *writeBuffer, uint32_t address, uint8_t writeSize);
uint32_t AT25Mxx_read_data(uint8_t *readBuffer, uint32_t address, uint8_t readSize);

/* ********** ABSTRACTION LAYER ********** */
/*  def */
#define EEPROM_SIZE 262144 //BYTES
#define EEPROM_MAX_ADDRESS 262140	//32 Bits (4 Bytes) for each variable)
/* Variables */

/* Functions */
void init_eeprom(void);
/* All functions perform 32 bit operations */
void eeprom_write_dword(uint32_t address_idx, uint32_t data);
uint32_t eeprom_read_dword(uint32_t address_idx);

// Page Size 256 bytes (64 x 32 bit variables)
void eeprom_write_page(uint32_t address, uint32_t *dataBuffer, uint8_t writeSize);
void eeprom_read_page(uint32_t address, uint32_t *dataBuffer, uint8_t readSize);

// static void eeprom_write_byte(uint32_t address, uint8_t data);
// static uint8_t eeprom_read_byte(uint32_t address);


/* ********** ********** */



#endif /* EEPROM_H_ */