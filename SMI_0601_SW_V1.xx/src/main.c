/**
 * \file
 *
 * \brief Empty user application template
 *
 */
#include <asf.h>
#include "main.h"
#include "led.h"
#include "push_button.h"
#include "device_config.h"
#include "AT25M02.h"
#include "eemem_address.h"
#include "AD5683RBRMZ.h"
#include "commands.h"
#include "stimulation.h"
#include "trigger_input.h"

/* Global variables */
led red_led, green_led, blue_led;
trigger_in start_abort_trigger;
push_button pb1, pb2;
Pdc *uart_pdc;
pdc_packet_t uart_pdc_packet;
char uart_pdc_buffer[UART_BUFFER_SIZE] ;
uint16_t command, data;
uint32_t temp_com_data, command_ack;

uint8_t adc_channel;
uint32_t adc_data, load_adc_data, load_adc_data_accum = 0, load_adc_counter = 0, latest_load_adc_val;
//uint32_t shunt_adc_data, shunt_adc_data_accum = 0, shunt_adc_counter = 0, latest_shunt_adc_val;
float load_voltage;
uint32_t rtt_counter = 0;
uint16_t starting_current, rmm_scan_35k, rmm_scan_300k, rmm_stim_10v, rmm_stim_25v;

int32_t new_dac_value;


/* External global variables */
extern enum _device_status device_status;
extern enum _stim_status stim_status;
extern uint16_t stim_intensity, stim_duration, sham_setting, time_remaining, stim_time_counter;
extern uint16_t ramp_delta_dac_value;
extern uint16_t last_dac_value;
extern bool start_abort_flag;

/*
// Kalman Filter for Shunt voltage
uint16_t shunt_kalman_filter(uint16_t adc_value){
	static float shunt_A = 1.00f, shunt_H = 1.00f, shunt_Q = 0.01f, shunt_R = 0.80f, shunt_P = 6.00f;
	static float shunt_XP, shunt_PP, shunt_value_return, shunt_K, shunt_temp_float;
	
	shunt_XP = shunt_A*shunt_value_return;
	shunt_PP = shunt_A*shunt_P*shunt_A + shunt_Q;
	
	shunt_K = shunt_PP*shunt_H;
	shunt_K /= (shunt_H*shunt_H*shunt_PP)+shunt_R;
	
	shunt_temp_float = ((float)adc_value-shunt_H*shunt_XP);
	shunt_value_return = shunt_XP + shunt_K*shunt_temp_float;

	shunt_temp_float = (float)shunt_H*shunt_PP;
	shunt_P = shunt_PP-shunt_K*shunt_temp_float;
	return (uint16_t)shunt_value_return;
}*/

uint16_t load_kalman_filter(uint16_t adc_value){
	static float load_A = 1.00f, load_H = 1.00f, load_Q = 0.01f, load_R = 0.80f, load_P = 6.00f;
	static float load_XP, load_PP, load_value_return, load_K, load_temp_float;
	
	load_XP = load_A*load_value_return;
	load_PP = load_A*load_P*load_A + load_Q;
	
	load_K = load_PP*load_H;
	load_K /= (load_H*load_H*load_PP)+load_R;
	
	load_temp_float = ((float)adc_value-load_H*load_XP);
	load_value_return = load_XP + load_K*load_temp_float;

	load_temp_float = (float)load_H*load_PP;
	load_P = load_PP-load_K*load_temp_float;
	return (uint16_t)load_value_return;
}

/* Interrupt handlers */
/* Uart handler */
void UART_Handler(void){
	if ((uart_get_status(UART_BASE) & UART_SR_RXBUFF) == UART_SR_RXBUFF) {
		pdc_rx_init(uart_pdc, &uart_pdc_packet, NULL);
		
		temp_com_data = strtoul(uart_pdc_buffer, NULL, 16);
		command = (temp_com_data>>16);
		data = (uint16_t)temp_com_data;
		
		command_ack = process_command(command, data);
		
		printf("%08lx", command_ack);
		printf("\n");
	}
}

void ADC_Handler(void){
	if((adc_get_status(ADC) & ADC_ISR_DRDY)== ADC_ISR_DRDY){
		adc_data = adc_get_latest_value(ADC);
		adc_channel = (adc_data & ADC_LCDR_CHNB_Msk) >> ADC_LCDR_CHNB_Pos;
		/*
		if(adc_channel == SHUNT_ADC_CHANNEL){
			shunt_adc_data = adc_data & ADC_LCDR_LDATA_Msk;
			if(shunt_adc_counter < ADC_SAMPLE_COUNT){
				shunt_adc_data_accum += shunt_adc_data;
				shunt_adc_counter++;
				return;
			}
			shunt_adc_data = (shunt_adc_data_accum/shunt_adc_counter);
			latest_shunt_adc_val = shunt_kalman_filter(shunt_adc_data);
			shunt_adc_counter = 0;
			shunt_adc_data_accum = 0;
			
		}
		else 
		*/
		if(adc_channel == LOAD_ADC_CHANNEL){
			load_adc_data = adc_data & ADC_LCDR_LDATA_Msk;
			if(load_adc_counter < ADC_SAMPLE_COUNT){
				load_adc_data_accum += load_adc_data;
				load_adc_counter++;
				return;
			}
			load_adc_data = (load_adc_data_accum/load_adc_counter);
			latest_load_adc_val = load_kalman_filter(load_adc_data);
			load_adc_counter = 0;
			load_adc_data_accum = 0;
		}
	}
}

/**
 * \brief TC0 Handler Interrupt handler for TC0, channel 0
 * \note Sends resistance measurement data to UART and controls LED
 */
bool led_blink = false;
void TC0_Handler(){
	tc_get_status(TC0, 0);
	if(device_status != IDLE && device_status != POST_STIMULATION){
		//printf("%04X%04X\n",(unsigned int)RMM_DATA, (unsigned int)latest_load_adc_val);
		if(device_status == PRE_STIMULATION || stim_status == SHAM_STIM){
			led_blink = !led_blink;
			if(led_blink){
				set_led_state(&green_led, LED_OFF);
				set_led_state(&red_led, LED_OFF);
				return;
			}
			if(latest_load_adc_val <= rmm_scan_35k){
				set_led_state(&green_led, LED_ON);
				set_led_state(&red_led, LED_OFF);
			}
			else if(latest_load_adc_val > rmm_scan_35k && latest_load_adc_val <= rmm_scan_300k){
				set_led_state(&green_led, LED_ON);
				set_led_state(&red_led, LED_ON);
			}
			else if(latest_load_adc_val > rmm_scan_300k){
				set_led_state(&green_led, LED_OFF);
				set_led_state(&red_led, LED_ON);
			}
		}
		else if(stim_status == END_OF_STIM){
			set_led_state(&green_led, LED_OFF);
			set_led_state(&red_led, LED_OFF);
		}
		else{	// active stim and ramps
			if(latest_load_adc_val <= rmm_stim_10v){
				set_led_state(&green_led, LED_ON);
				set_led_state(&red_led, LED_OFF);
			}
			else if(latest_load_adc_val > rmm_stim_10v && latest_load_adc_val <= rmm_stim_25v){
				set_led_state(&green_led, LED_ON);
				set_led_state(&red_led, LED_ON);
			}
			else if(latest_load_adc_val > rmm_stim_25v){
				set_led_state(&green_led, LED_OFF);
				set_led_state(&red_led, LED_ON);
			}
		}
	}
	else if(device_status == POST_STIMULATION){
		toggle_led_state(&red_led);
		set_led_state(&green_led, LED_OFF);
	}
	else{
		set_led_state(&green_led, LED_OFF);
		set_led_state(&red_led, LED_OFF);
	}
}

/**
 * \brief TC1 Handler, Interrupt handler for TC0, channel 1
 * \note Control DAC based on stimulation flag (CURRENT CONTROLLER)
 */
void TC1_Handler(){
	tc_get_status(TC0, 1);
	if(device_status == IDLE){
		set_dac_value(0);		// Lowest DAC value
	}
	else if(device_status == PRE_STIMULATION){
		set_dac_value(starting_current);
	}
	else if(device_status == STIMULATION){
		// Control Stimulation related stuff here
		
		if(stim_status == RAMP_UP || stim_status == SHAM_RAMP_UP){
			new_dac_value = (int32_t)(last_dac_value + ramp_delta_dac_value);
			if(new_dac_value > MAX_DAC_VALUE){
				new_dac_value = MAX_DAC_VALUE;
			}
			set_dac_value((uint16_t)new_dac_value);
		}
		else if(stim_status == RAMP_DOWN || stim_status == SHAM_RAMP_DOWN){
			new_dac_value = (int32_t)(last_dac_value - ramp_delta_dac_value);
			if(new_dac_value < 0){
				new_dac_value = 0;
			}
			set_dac_value((uint16_t)new_dac_value);
		}
		else if(stim_status == ACTIVE_STIM){
			set_dac_value(last_dac_value);
		}
		else if(stim_status == SHAM_STIM){
			set_dac_value(starting_current);
		}
		else if(stim_status == END_OF_STIM){
			set_dac_value(0);
		}
	}
	else if(device_status == POST_STIMULATION){
		set_dac_value(0);
	}
	else if(device_status == CURRENT_DATA_UPDATE){
		// Do nothing
	}
}

/**
 * \brief 
 * \Note Change device status based on push button press or trigger
 */
uint32_t pb1_hold_dur = 0, pb2_hold_dur = 0;	// To detect long press
uint32_t tc0_2_counter = 0;
uint32_t trigger_hold_dur = 0;

void TC2_Handler(){
	tc_get_status(TC0, 2);
	if(device_status == IDLE){
		toggle_led_state(&blue_led);
		tc0_2_counter = 0;
	}
	else if(device_status == POST_STIMULATION){
		set_led_state(&blue_led, LED_OFF);
	}
	else{
		tc0_2_counter++;
		if(tc0_2_counter == 25){
			set_led_state(&blue_led, LED_ON);	
		}
		else if(tc0_2_counter == 26){
			set_led_state(&blue_led, LED_OFF);
			tc0_2_counter = 0;
		}
		else{
			set_led_state(&blue_led, LED_OFF);
		}
	}
	
	// Check buttons and trigger status
	if(is_pb_pressed(pb1)){
		if(pb1_hold_dur == PB_DEBOUNCE_TIME){
			if(device_status == POST_STIMULATION){
				rstc_start_software_reset(RSTC);
			}
			else{
				start_abort_flag = true;
			}
		}
		pb1_hold_dur += TC0_2_PERIOD;
	}
	else{
		pb1_hold_dur = 0;
	}
	
	if(check_trigger(start_abort_trigger) == TRIGGERED){
		if(trigger_hold_dur	== TRIGGER_DEBOUNCE_TIME){
			start_abort_flag = true;
		}
		trigger_hold_dur += TC0_2_PERIOD;
	}
	else{
		trigger_hold_dur = 0;
	}
	
	if(start_abort_flag){
		if(device_status == IDLE){
			device_status = PRE_STIMULATION;
		}
		else if(device_status == PRE_STIMULATION){
			stimulation();		// Sets up device for stimulation
			device_status = STIMULATION;
		}
		else if(device_status == STIMULATION){	// During stimulation
			if(stim_status == RAMP_UP){
				stim_time_counter = (stim_duration - stim_time_counter);
			}
			else if(stim_status == RAMP_DOWN){
				// No action required. Stimulation will end after ramp down
			}
			else if(stim_status == ACTIVE_STIM){
				stim_time_counter = stim_duration - RAMP_DURATION;
			}
			else if(stim_status == SHAM_RAMP_UP){
				// stim_time_counter = stim_duration -(stim_time_counter - (stim_duration - (2*RAMP_DURATION)
				// Simplified version of above
				stim_time_counter = (2*stim_duration)-stim_time_counter-(2*RAMP_DURATION);
			}
			else if(stim_status == SHAM_RAMP_DOWN){
				stim_time_counter = stim_time_counter+(stim_duration-(2*RAMP_DURATION));
			}
			else if(stim_status == SHAM_STIM){
				stim_time_counter = stim_duration - RAMP_DURATION;
			}
		}
		start_abort_flag = false;
	}
}

/**
 * \brief RTT Handler used as main clock (1 Hz)
 */
void RTT_Handler(){
	rtt_get_status(RTT);
	rtt_counter++;
	if(device_status == STIMULATION){
		stim_time_counter++;
		if(sham_setting == SHAM_OFF){
			if(stim_time_counter < RAMP_DURATION){
				stim_status = RAMP_UP;
			}
			else if(stim_time_counter < (stim_duration - RAMP_DURATION)){
				stim_status = ACTIVE_STIM;
			}
			else if(stim_time_counter < stim_duration){
				stim_status = RAMP_DOWN;
			}
			else{
				stim_status = END_OF_STIM;
				device_status = POST_STIMULATION;
			}
		}
		else{		// Sham on
			if(stim_time_counter < RAMP_DURATION){
				stim_status = RAMP_UP;
			}
			else if(stim_time_counter < (RAMP_DURATION*2)){
				stim_status = SHAM_RAMP_DOWN;
			}
			else if(stim_time_counter < (stim_duration - (RAMP_DURATION*2))){
				stim_status = SHAM_STIM;
			}
			else if(stim_time_counter < (stim_duration - RAMP_DURATION)){
				stim_status = SHAM_RAMP_UP;
			}
			else if(stim_time_counter < stim_duration){
				stim_status = RAMP_DOWN;
			}
			else{
				stim_status = END_OF_STIM;
				device_status = POST_STIMULATION;
			}
		}
		
	}
}

/**
 * \brief main
 */
int main (void){
	init_device();
	/* Load starting current value from EEPROM */
	starting_current = (uint16_t)eeprom_read_dword(EEMEM_25UA);
	rmm_scan_35k = (uint16_t)eeprom_read_dword(EEMEM_SCAN_RMM_35K);
	rmm_scan_300k = (uint16_t)eeprom_read_dword(EEMEM_SCAN_RMM_300K);
	rmm_stim_10v = (uint16_t)eeprom_read_dword(EEMEM_STIM_RMM_10V);
	rmm_stim_25v = (uint16_t)eeprom_read_dword(EEMEM_STIM_RMM_25V);
	
	while(1){
		
	}
}
