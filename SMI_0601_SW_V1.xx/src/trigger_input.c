/*
 * trigger_input.c
 *
 * Created: 5/12/2020 05:02:46 PM
 *  Author: mdev_soterix
 */ 

#include "trigger_input.h"

/*
 * \brief Initializes the trigger
 * \param trigger trigger_in structure
 */
void init_trigger(trigger_in trigger){
	// Initialize the pin
	ioport_set_pin_dir(trigger.trigger_in_pin, IOPORT_DIR_INPUT);
	// Set pull up or pull down based on logic level
	if(trigger.trigger_in_logic_level == TRIGGER_ACTIVE_HIGH){
		ioport_set_pin_mode(trigger.trigger_in_pin, IOPORT_MODE_PULLDOWN);
	}
	else if(trigger.trigger_in_logic_level == TRIGGER_ACTIVE_LOW){
		ioport_set_pin_mode(trigger.trigger_in_pin, IOPORT_MODE_PULLUP);
	}
	else{
		// error: shouldn't be here
	}
}

/*
 * \brief Read and return trigger status
 * \param trigger trigger_in structure
 * \return trigger_status enum trigger status
 */
enum trigger_status check_trigger(trigger_in trigger){
	if(trigger.trigger_in_logic_level == TRIGGER_ACTIVE_HIGH){
		return ioport_get_pin_level(trigger.trigger_in_pin);
	}
	else if(trigger.trigger_in_logic_level == TRIGGER_ACTIVE_LOW){
		return !(ioport_get_pin_level(trigger.trigger_in_pin));
	}
	else{
		// error: shouldn't be here
		return NOT_TRIGGERED;
	}
}