/*
 * commands.c
 *
 * Created: 5/22/2020 03:11:26 PM
 *  Author: mdev_soterix
 */ 

#include <asf.h>
#include "main.h"
#include "AT25M02.h"
#include "eemem_address.h"
#include "commands.h"
#include "AD5683RBRMZ.h"
#include "stimulation.h"

extern enum _device_status device_status;
extern bool start_abort_flag;
extern uint32_t latest_load_adc_val;
//extern enum _stim_status stim_status;

uint32_t process_command(uint16_t command, uint16_t data){
	if(command == NO_OP){
		return ACK_NO_OP_OK;
	}
	else if(command == COM_CHECK){
		return ACK_COM_CHECK_OK;
	}
	else if(command == GET_SERIAL_NUMBER){
		return eeprom_read_dword(EEMEM_DEVICE_SERIAL_NUMBER);
	}
	else if(command == GET_FIRMWARE_VERSION){
		uint32_t firmware_ver = (GET_FIRMWARE_VERSION<<16) | FIRMWARE_VERSION;
		return firmware_ver;
	}
	else if(command == SET_STIM_INTENSITY){
		eeprom_write_dword(EEMEM_CURRENT_INTENSITY, data);
		return ACK_SET_STIM_INTENSITY_OK;
	}
	else if(command == SET_STIM_DURATION){
		eeprom_write_dword(EEMEM_CURRENT_DURATION, data);
		return ACK_SET_STIM_DURATION_OK;
	}
	else if(command == SET_SHAM_SETTING){
		eeprom_write_dword(EEMEM_CURRENT_SHAM, data);
		return ACK_SET_SHAM_SETTING_OK;
	}
	else if(command == GET_STIM_INTENSITY){
		uint32_t current_intensity = eeprom_read_dword(EEMEM_CURRENT_INTENSITY);
		current_intensity |= 0x04010000;
		return current_intensity;
	}
	else if(command == GET_STIM_DURATION){
		uint32_t current_duration = eeprom_read_dword(EEMEM_CURRENT_DURATION);
		current_duration |= 0x04020000;
		return current_duration;
	}
	else if(command == GET_SHAM_SETTING){
		uint32_t current_sham = eeprom_read_dword(EEMEM_CURRENT_SHAM);
		current_sham |= 0x04030000;
		return current_sham;
	}
	else if(command == START_RMM){
		device_status = PRE_STIMULATION;
		return ACK_START_RMM_OK;
	}
	else if(command == GET_RMM_DATA){
		return (latest_load_adc_val | (GET_RMM_DATA<<16));
	}
	else if(command == STOP_RMM){
		device_status = IDLE;
		return ACK_STOP_RMM_OK;
	}
	else if(command == START_RMM_CALIB){
		device_status = RMM_CALIBRATION;
		return ACK_OK;
	}
	else if(command == STOP_RMM_CALIB){
		device_status = IDLE;
		return ACK_OK;
	}
	else if(command == SET_SERIAL_NUMBER_H){
		volatile uint32_t serial_number = (data<<16);
		eeprom_write_dword(EEMEM_DEVICE_SERIAL_NUMBER, serial_number);
		return ACK_SET_SERIAL_NUMBER_H_OK;
	}
	else if(command == SET_SERIAL_NUMBER_L){
		volatile uint32_t serial_number = eeprom_read_dword(EEMEM_DEVICE_SERIAL_NUMBER);
		serial_number = serial_number | data;
		eeprom_write_dword(EEMEM_DEVICE_SERIAL_NUMBER, serial_number);
		return ACK_SET_SERIAL_NUMBER_L_OK;
	}
	else if(command == CALIB_UPDATE_DAC){
		device_status = CURRENT_DATA_UPDATE;
		set_dac_value(data);
		return ACK_OK;
	}
	else if(command == SAVE_25UA_VALUE){
		eeprom_write_dword(EEMEM_25UA, data);
		return ACK_OK;
	}
	else if(command == SAVE_500UA_VALUE){
		eeprom_write_dword(EEMEM_500UA, data);
		return ACK_OK;
	}
	else if(command == SAVE_1000UA_VALUE){
		eeprom_write_dword(EEMEM_1000UA, data);
		return ACK_OK;
	}
	else if(command == SAVE_1500UA_VALUE){
		eeprom_write_dword(EEMEM_1500UA, data);
		return ACK_OK;
	}
	else if(command == SAVE_2000UA_VALUE){
		eeprom_write_dword(EEMEM_2000UA, data);
		return ACK_OK;
	}
	else if(command == SAVE_2500UA_VALUE){
		eeprom_write_dword(EEMEM_2500UA, data);
		return ACK_OK;
	}
	else if(command == UPDATE_DAC_25UA){
		uint32_t dac_val = eeprom_read_dword(EEMEM_25UA);
		set_dac_value(dac_val);
		return ACK_OK;
	}
	else if(command == UPDATE_DAC_500UA){
		uint32_t dac_val = eeprom_read_dword(EEMEM_500UA);
		set_dac_value(dac_val);
		return ACK_OK;
	}
	else if(command == UPDATE_DAC_1000UA){
		uint32_t dac_val = eeprom_read_dword(EEMEM_1000UA);
		set_dac_value(dac_val);
		return ACK_OK;
	}
	else if(command == UPDATE_DAC_1500UA){
		uint32_t dac_val = eeprom_read_dword(EEMEM_1500UA);
		set_dac_value(dac_val);
		return ACK_OK;
	}
	else if(command == UPDATE_DAC_2000UA){
		uint32_t dac_val = eeprom_read_dword(EEMEM_2000UA);
		set_dac_value(dac_val);
		return ACK_OK;
	}
	else if(command == UPDATE_DAC_2500UA){
		uint32_t dac_val = eeprom_read_dword(EEMEM_2500UA);
		set_dac_value(dac_val);
		return ACK_OK;
	}
	else if(command == GET_SCAN_RMM_35K){
		return eeprom_read_dword(EEMEM_SCAN_RMM_35K);
	}
	else if(command == GET_SCAN_RMM_300K){
		return eeprom_read_dword(EEMEM_SCAN_RMM_300K);
	}
	else if(command == GET_STIM_RMM_10V){
		return eeprom_read_dword(EEMEM_STIM_RMM_10V);
	}
	else if(command == GET_STIM_RMM_25V){
		return eeprom_read_dword(EEMEM_STIM_RMM_25V);
	}
	else if(command == SAVE_SCAN_RMM_35K){
		eeprom_write_dword(EEMEM_SCAN_RMM_35K, data);
		return ACK_OK;
	}
	else if(command == SAVE_SCAN_RMM_300K){
		eeprom_write_dword(EEMEM_SCAN_RMM_300K, data);
		return ACK_OK;
	}
	else if(command == SAVE_STIM_RMM_10V){
		eeprom_write_dword(EEMEM_STIM_RMM_10V, data);
		return ACK_OK;
	}
	else if(command == SAVE_STIM_RMM_25V){
		eeprom_write_dword(EEMEM_STIM_RMM_25V, data);
		return ACK_OK;
	}
	else if(command == START_STIMULATION){
		start_abort_flag = true;
		return ACK_OK;
	}
	else if(command == ABORT_STIMULATION){
		start_abort_flag = true;
		return ACK_OK;
	}
	
	return DEVICE_ERR;
}