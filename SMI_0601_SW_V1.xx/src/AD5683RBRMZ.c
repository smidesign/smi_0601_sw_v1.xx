/*
 * AD5683RBRMZ.c
 *
 * Created: 1/28/2016 12:52:45 PM
 *  Author: Mithlesh
 */ 

#include "AD5683RBRMZ.h"

uint16_t last_dac_value;
struct spi_device device_AD5683 = { .id = AD5683_SPI_CHIP_SEL };

/**
 *\brief Initialize SPI for the device
 */
void init_AD5683(void){	
	spi_flags_t spi_flags_AD5683 = AD5683_SPI_MODE;
	board_spi_select_id_t AD5683_select_id = AD5683_SPI_CHIP_SEL;
	spi_master_setup_device(AD5683_SPI_MASTER_BASE, &device_AD5683, spi_flags_AD5683, 
							AD5683_SPI_CLOCK_SPEED, AD5683_select_id);
}

void set_dac_value(uint16_t dac_value){
	last_dac_value = dac_value;
	uint32_t data = dac_value;
	data = data<<12;
	data += 0x30000000;
	
	uint8_t write_buffer[3];
	write_buffer[0] = data>>24;
	write_buffer[1] = data>>16;
	write_buffer[2] = data>>8;
	
	spi_select_device(AD5683_SPI_MASTER_BASE, &device_AD5683);
	spi_write_packet(AD5683_SPI_MASTER_BASE, write_buffer, 3);
	spi_deselect_device(AD5683_SPI_MASTER_BASE, &device_AD5683);
}
