/*
 * stimulation.c
 *
 * Created: 5/30/2020 02:20:38 PM
 *  Author: mdev_soterix
 */ 
#include <asf.h>
#include "stimulation.h"
#include "device_config.h"
#include "AT25M02.h"
#include "eemem_address.h"
#include "AD5683RBRMZ.h"

/* Global Variables */
uint16_t stim_intensity, stim_duration, sham_setting, time_remaining, stim_time_counter;
uint16_t ramp_delta_dac_value;				// Intensity increment/decrement during ramps

extern uint16_t last_dac_value;

/* Stim flags (GLOBAL) */
enum _device_status device_status = IDLE;
enum _stim_status stim_status = PRE_STIMULATION;
bool start_abort_flag = false;

void stimulation(){
	// Read stimulation parameters
	stim_intensity = (uint16_t)eeprom_read_dword(EEMEM_CURRENT_INTENSITY);
	if(stim_intensity == 500){
		stim_intensity = (uint16_t)eeprom_read_dword(EEMEM_500UA);
	}
	else if(stim_intensity == 1000){
		stim_intensity = (uint16_t)eeprom_read_dword(EEMEM_1000UA);
	}
	else if(stim_intensity == 1500){
		stim_intensity = (uint16_t)eeprom_read_dword(EEMEM_1500UA);
	}
	else if(stim_intensity == 2000){
		stim_intensity = (uint16_t)eeprom_read_dword(EEMEM_2000UA);
	}
	else if(stim_intensity == 2500){
		stim_intensity = (uint16_t)eeprom_read_dword(EEMEM_2500UA);
	}
	stim_duration = (uint16_t)eeprom_read_dword(EEMEM_CURRENT_DURATION);
	time_remaining = stim_duration;
	stim_time_counter = 0;
	sham_setting = (uint16_t)eeprom_read_dword(EEMEM_CURRENT_SHAM);
	ramp_delta_dac_value = (stim_intensity/(RAMP_DURATION*TC0_1_FREQUENCY));
	
	set_dac_value(0);
	start_abort_flag = false;
}