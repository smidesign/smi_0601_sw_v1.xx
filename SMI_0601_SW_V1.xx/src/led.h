/*
 * led.h
 *
 * Created: 5/12/2020 04:24:15 PM
 *  Author: mdev_soterix
 */ 


#ifndef LED_H_
#define LED_H_

#include <asf.h>


/* */
enum _led_logic_level{
	LED_ACTIVE_HIGH,
	LED_ACTIVE_LOW
	};
	
enum _led_state{
	LED_OFF,
	LED_ON
	};

typedef struct led{
	uint32_t led_pin;
	enum _led_state led_state;
	enum _led_logic_level led_logic_level;
}led;

/* Functions */
void init_led(led *_led);
void set_led_state(led *_led, enum _led_state led_state);
void toggle_led_state(led *_led);
enum _led_state get_led_state(led _led);

#endif /* LED_H_ */