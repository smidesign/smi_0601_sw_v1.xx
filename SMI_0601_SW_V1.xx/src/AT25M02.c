/*
 * eeprom.c
 *
 * Created: 5/15/2018 11:54:28 AM
 *  Author: Mithlesh
 */ 

#include <asf.h>
#include "AT25M02.h"
#include "eemem_address.h"	//Provides address for each variable

struct spi_device device_AT25M02 = { .id = AT25M02_SPI_CHIP_SEL };

void init_AT25M02()
{
	//Configure all non SPI pins
	ioport_set_pin_dir(AT25M02_N_WP_PIN, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(AT25M02_N_HOLD_PIN, IOPORT_DIR_OUTPUT);
	
	DISABLE_EEPROM_WP;		//Disable the write protect
	DISABLE_EEPROM_HOLD;	//Disable the hold
	
	spi_flags_t spi_flags_AT25M02 = AT25M02_SPI_MODE;
	board_spi_select_id_t AT25M02_select_id = AT25M02_SPI_CHIP_SEL;
	spi_master_setup_device(AT25M02_SPI_MASTER_BASE, &device_AT25M02, spi_flags_AT25M02,
							AT25M02_SPI_CLOCK_SPEED, AT25M02_select_id);
}

void AT25Mxx_write_enable(void){
	uint8_t AT25M02_write_buffer[1] = {	AT25M02_WREN };
	spi_select_device(AT25M02_SPI_MASTER_BASE, &device_AT25M02);
	spi_write_packet(AT25M02_SPI_MASTER_BASE, AT25M02_write_buffer, 1);
	spi_deselect_device(AT25M02_SPI_MASTER_BASE, &device_AT25M02);
}

void AT25Mxx_write_disable(void){
	uint8_t AT25M02_write_buffer[1] = { AT25M02_WRDI };
	spi_select_device(AT25M02_SPI_MASTER_BASE, &device_AT25M02);
	spi_write_packet(AT25M02_SPI_MASTER_BASE, AT25M02_write_buffer, 1);
	spi_deselect_device(AT25M02_SPI_MASTER_BASE, &device_AT25M02);
}

uint8_t AT25Mxx_read_SR(void){
	uint8_t AT25M02_write_buffer[1] = { AT25M02_RDSR };
	uint8_t sr_val;
	spi_select_device(AT25M02_SPI_MASTER_BASE, &device_AT25M02);
	spi_write_packet(AT25M02_SPI_MASTER_BASE, AT25M02_write_buffer, 1);
	spi_read_packet(AT25M02_SPI_MASTER_BASE, &sr_val, 1);
	spi_deselect_device(AT25M02_SPI_MASTER_BASE, &device_AT25M02);
	return sr_val;
}

void AT25Mxx_write_SR(uint8_t data){
	uint8_t AT25M02_write_buffer[2];
	AT25M02_write_buffer[0] = AT25M02_WRSR;
	AT25M02_write_buffer[1] = data;
	spi_select_device(AT25M02_SPI_MASTER_BASE, &device_AT25M02);
	spi_write_packet(AT25M02_SPI_MASTER_BASE, AT25M02_write_buffer, 2);
	spi_deselect_device(AT25M02_SPI_MASTER_BASE, &device_AT25M02);
}

uint32_t AT25Mxx_busy_wait(void){
	uint8_t SRval = 0xFF;
	while((SRval & AT25M02_SR_BSY_MSK) == AT25M02_SR_BSY){
		SRval = AT25Mxx_read_SR();
	}
	return 0;
}

uint32_t AT25Mxx_LPWP_wait(void){
	uint8_t pollVal = AT25M02_LPWP_BSY;
	uint8_t AT25M02_write_buffer[1] = { AT25M02_LPWP };
	spi_select_device(AT25M02_SPI_MASTER_BASE, &device_AT25M02);
	while(pollVal == AT25M02_LPWP_BSY){
		spi_write_packet(AT25M02_SPI_MASTER_BASE, AT25M02_write_buffer, 1);
		spi_read_packet(AT25M02_SPI_MASTER_BASE, &pollVal, 1);
	}
	spi_deselect_device(AT25M02_SPI_MASTER_BASE, &device_AT25M02);
	return 0;
}
/*
uint32_t AT25Mxx_write_data(uint8_t *writeBuffer, uint32_t address, uint8_t writeSize)
{
	AT25Mxx_write_enable();
	AT25Mxx_LPWP_wait();
	spi_select_device(AT25M02_SPI_MASTER_BASE, &device_AT25M02);

	uint8_t tempData ;
	
	uint8_t addressByte[3];
	addressByte[0] = (uint8_t)(address >> 16);
	addressByte[1] = (uint8_t)(address >> 8);
	addressByte[2] = (uint8_t)(address);
	
	txTimeoutWait(SPI_TIMEOUT);
	spi_put(AT25M02_SPI_MASTER_BASE, (uint16_t)AT25M02_WRITE);
	
	for(uint8_t i = 0; i < 3; i++){
		txTimeoutWait(SPI_TIMEOUT);
		spi_put(AT25M02_SPI_MASTER_BASE, (uint16_t)addressByte[i]);
	}
	
	for(uint32_t i = 0; i < writeSize; i++)
	{
		txTimeoutWait(SPI_TIMEOUT);
		tempData = writeBuffer[i];
		spi_put(SPI, (uint16_t)tempData);
	}
	spi_deselect_device(AT25M02_SPI_MASTER_BASE, &device_AT25M02);
	AT25Mxx_LPWP_wait();
	AT25Mxx_busy_wait();
	return 0;
}

uint32_t AT25Mxx_read_data(uint8_t *readBuffer, uint32_t address, uint8_t readSize)
{
	AT25Mxx_write_enable();
	AT25Mxx_LPWP_wait();
	
	spi_select_device(AT25M02_SPI_MASTER_BASE, &device_AT25M02);
	uint8_t tempVal;

	uint8_t addressByte[3];
	addressByte[0] = (uint8_t)(address >> 16);
	addressByte[1] = (uint8_t)(address >> 8);
	addressByte[2] = (uint8_t)(address);
	
	txTimeoutWait(SPI_TIMEOUT);
	spi_put(AT25M02_SPI_MASTER_BASE, (uint16_t)AT25M02_READ);
	
	for(uint8_t i = 0; i < 3; i++){
		txTimeoutWait(SPI_TIMEOUT);
		spi_put(AT25M02_SPI_MASTER_BASE, (uint16_t)addressByte[i]);
	}
	
	for(uint32_t i = 0; i < readSize; i++)
	{
		txTimeoutWait(SPI_TIMEOUT);
		spi_put(AT25M02_SPI_MASTER_BASE, 0x00);	//Dummy send
		
		rxTimeoutWait(SPI_TIMEOUT);
		
		tempVal = (uint8_t)spi_get(AT25M02_SPI_MASTER_BASE);
		readBuffer[i] = tempVal;
		
	}
	spi_deselect_device(AT25M02_SPI_MASTER_BASE, &device_AT25M02);
	
	AT25Mxx_LPWP_wait();
	AT25Mxx_busy_wait();

	return 0;
}
*/

/* ********** ABSTRACTION LAYER ********** 
void init_eeprom(void){
	initAT25M02();
}
*/

static void eeprom_write_byte(uint32_t address, uint8_t data){
	AT25Mxx_write_enable();
	AT25Mxx_LPWP_wait();
	uint8_t AT25M02_write_buffer[5];
	spi_select_device(AT25M02_SPI_MASTER_BASE, &device_AT25M02);

	AT25M02_write_buffer[0] = AT25M02_WRITE;
	AT25M02_write_buffer[1] = (uint8_t)(address >> 16);
	AT25M02_write_buffer[2] = (uint8_t)(address >> 8);
	AT25M02_write_buffer[3] = (uint8_t)(address);
	AT25M02_write_buffer[4] = data;
	
	spi_write_packet(AT25M02_SPI_MASTER_BASE, AT25M02_write_buffer, 5);
	spi_deselect_device(AT25M02_SPI_MASTER_BASE, &device_AT25M02);
	
	AT25Mxx_LPWP_wait();
	AT25Mxx_busy_wait();
}

static uint8_t eeprom_read_byte(uint32_t address){
	//AT25Mxx_write_enable();
	//AT25Mxx_LPWP_wait();
	uint8_t AT25M02_write_buffer[4];
	spi_select_device(AT25M02_SPI_MASTER_BASE, &device_AT25M02);

	AT25M02_write_buffer[0] = AT25M02_READ;
	AT25M02_write_buffer[1] = (uint8_t)(address >> 16);
	AT25M02_write_buffer[2] = (uint8_t)(address >> 8);
	AT25M02_write_buffer[3] = (uint8_t)(address);
	
	uint8_t read_byte;
	spi_write_packet(AT25M02_SPI_MASTER_BASE, AT25M02_write_buffer, 4);
	spi_read_packet(AT25M02_SPI_MASTER_BASE, &read_byte, 1);
	spi_deselect_device(AT25M02_SPI_MASTER_BASE, &device_AT25M02);
	
	AT25Mxx_LPWP_wait();
	AT25Mxx_busy_wait();
	return read_byte;
}

void eeprom_write_dword(uint32_t address_idx, uint32_t data){
	uint32_t address = address_idx*4;
	uint8_t dataBuffer[4];
	dataBuffer[0] = (uint8_t)(data >> 24);
	dataBuffer[1] = (uint8_t)(data >> 16);
	dataBuffer[2] = (uint8_t)(data >> 8);
	dataBuffer[3] = (uint8_t)(data);
	
	for(uint8_t i = 0; i < 4; i++){
		eeprom_write_byte((address+i),dataBuffer[i]);
	}
}

uint32_t eeprom_read_dword(uint32_t address_idx){
	uint8_t dataBuffer[4];
	uint32_t data;
	uint32_t address = address_idx*4;
	for(uint8_t i = 0; i < 4; i++){
		dataBuffer[i] = eeprom_read_byte(address+i);
	}
	
	data =  (uint32_t)((uint32_t)(dataBuffer[0] << 24) +
	(uint32_t)(dataBuffer[1] << 16) +
	(uint32_t)(dataBuffer[2] << 8) +
	(uint32_t)(dataBuffer[3]));
	return data;
}
/*
void eeprom_write_page(uint32_t address, uint32_t *dataBuffer, uint8_t writeSize){
	init_eeprom();
	uint8_t writeBuffer[256];
	uint8_t dataBufferIndex = 0;
	for(uint8_t i = 0; i < writeSize; i+=4){
		writeBuffer[i] = (uint8_t) (dataBuffer[dataBufferIndex]>>24);
		writeBuffer[i+1] = (uint8_t) (dataBuffer[dataBufferIndex]>>16);
		writeBuffer[i+2] = (uint8_t) (dataBuffer[dataBufferIndex]>>8);
		writeBuffer[i+3] = (uint8_t) (dataBuffer[dataBufferIndex]);
		dataBufferIndex++;
	}//for(uint8_t...
	//initAT25M02();
	AT25Mxx_write_data(writeBuffer, address, writeSize);
}

void eeprom_read_page(uint32_t address, uint32_t *dataBuffer, uint8_t readSize){
	init_eeprom();
	uint8_t readBuffer[256];
	uint8_t dataBufferIndex = 0;
	//initAT25M02();
	AT25Mxx_read_data(readBuffer, address, readSize);
	
	for(uint8_t i = 0; i < readSize; i+=4){
		dataBuffer[dataBufferIndex] =   (uint32_t)((uint32_t)(readBuffer[i] << 24) +
												   (uint32_t)(readBuffer[i+1] << 16) +
												   (uint32_t)(readBuffer[i+2] << 8) +
												   (uint32_t)(readBuffer[i+3]));
		dataBufferIndex++;
	}
}
 ********** ********** */