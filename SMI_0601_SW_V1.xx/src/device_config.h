/*
 * device_config.h
 *
 * Created: 5/18/2020 03:48:20 PM
 *  Author: mdev_soterix
 */ 


#ifndef DEVICE_CONFIG_H_
#define DEVICE_CONFIG_H_

#include <asf.h>
/* UART */
#define UART_RX_PIN			PIO_PB2_IDX
#define UART_TX_PIN			PIO_PB3_IDX

#define UART_ID				ID_UART1
#define UART_BASE			UART1
#define UART_IRQn			UART1_IRQn
#define UART_Handler		UART1_Handler
#define UART_BUFFER_SIZE	8	// 8 characters. Each character represents a hex digit

/* LED */
#define RED_LED_PIN		PIO_PA15_IDX
#define GREEN_LED_PIN	PIO_PA25_IDX
#define BLUE_LED_PIN	PIO_PA16_IDX

/* PUSH BUTTON */
#define PB1_PIN				PIO_PA1_IDX
#define PB2_PIN				PIO_PA2_IDX
#define PB_DEBOUNCE_TIME	TC0_2_PERIOD*1

/* TRIGGER IN*/
#define TRIGGER_IN_PIN			PIO_PA21_IDX
#define TRIGGER_DEBOUNCE_TIME	TC0_2_PERIOD
/* SPI */
// SPI pin definitions
#define SPI_MISO_PIN		PIO_PA12_IDX
#define SPI_MISO_PIN_FLAG	IOPORT_MODE_MUX_A
#define SPI_MOSI_PIN		PIO_PA13_IDX
#define SPI_MOSI_PIN_FLAG	IOPORT_MODE_MUX_A
#define SPI_SPCK_PIN		PIO_PA14_IDX
#define SPI_SPCK_PIN_FLAG	IOPORT_MODE_MUX_A

//SPI devices chip select
#define SPI_AD5683_NSS_PIN			PIO_PA11_IDX
#define SPI_AD5683_NSS_PIN_FLAG		IOPORT_MODE_MUX_A
#define SPI_AT25M02_NSS_PIN			PIO_PA5_IDX
#define SPI_AT25M02_NSS_PIN_FLAG	IOPORT_MODE_MUX_B
#define SPI_MASTER_BASE				SPI

/* ADC */
#define RMM_FREQUENCY			5
#define ADC_CLOCK_FREQUENCY		10000
#define ADC_SAMPLE_COUNT		1000
#define SHUNT_ADC_CHANNEL		ADC_CHANNEL_4	//PB0
#define LOAD_ADC_CHANNEL		ADC_CHANNEL_5	//PB1

/* TIMER FREQUENCY */
#define TC0_1_FREQUENCY			10
#define TC0_2_FREQUENCY			5
#define TC0_2_PERIOD			(1000/TC0_2_FREQUENCY)

/* Functions */
void init_device(void);
void config_uart_serial(void);
void init_leds(void);
void init_push_buttons(void);
void config_spi(void);
void init_tc0_0(uint32_t freq);
void init_tc0_1(uint32_t freq);
void init_tc0_2(uint32_t freq);
void init_adc(void);
void init_rtt(void);


#endif /* DEVICE_CONFIG_H_ */