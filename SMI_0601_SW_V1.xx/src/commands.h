/*
 * commands.h
 *
 * Created: 5/22/2020 12:46:39 PM
 *  Author: mdev_soterix
 */ 


#ifndef COMMANDS_H_
#define COMMANDS_H_

/* Commands 4 bytes 
 * First 2 bytes are commands
 * Last 2 bytes are data. Send 0x0000 if no data to be sent.
 * All commands will reply with ACK
 */
#define NO_OP						0x0000		// No operation. Nothing to do. Will reply NO_OP_OK
#define ACK_NO_OP_OK				0x00000001
#define ACK_OK						0x00000001

// Communication related
#define COM_CHECK					0x0100		// Communication check. Will reply COM_OK
#define ACK_COM_CHECK_OK			0x01000001
#define SET_BAUD_RATE				0x0101		// Send baud in data bytes
// Device specific
#define GET_FIRMWARE_VERSION		0x0200

#define GET_SERIAL_NUMBER			0x0201
#define SET_SERIAL_NUMBER_H			0x0202
#define ACK_SET_SERIAL_NUMBER_H_OK	0x02020001
#define ACK_SET_SERIAL_NUMBER_H_ERR	0x0202FFFF
#define SET_SERIAL_NUMBER_L			0x0203
#define ACK_SET_SERIAL_NUMBER_L_OK	0x02030001
#define ACK_SET_SERIAL_NUMBER_L_ERR	0x0203FFFF

// Change device settings (stim related)
#define SET_STIM_INTENSITY			0x0301		// Send intensity in uA in data bytes
#define ACK_SET_STIM_INTENSITY_OK	0x03010001
#define ACK_SET_STIM_INTENSITY_ERR	0x0301FFFF

#define SET_STIM_DURATION			0x0302		// Send duration in seconds in data bytes
#define ACK_SET_STIM_DURATION_OK	0x03020001
#define ACK_SET_STIM_DURATION_ERR	0x0302FFFF

#define SET_SHAM_SETTING			0x0303		// Send sham condition in data bytes. See sham definitions below
#define ACK_SET_SHAM_SETTING_OK		0x03030001
#define ACK_SET_SHAM_SETTING_ERR	0x0303FFFF
#define SHAM_SETTING_OFF			0x0000
#define SHAM_SETTING_ON				0x0001

// Read device settings (stim related)
#define GET_STIM_INTENSITY			0x0401		// Will reply with existing STIM INTENSITY
#define GET_STIM_DURATION			0x0402		// Will reply with existing STIM DURATION 
#define GET_SHAM_SETTING			0x0403		// Will reply with existing SHAM SETTING 

// Stimulation
#define START_RMM					0x0501		// Start resistance measurement
#define ACK_START_RMM_OK			0x05010001
#define ACK_START_RMM_ERR			0x0501FFFF

#define STOP_RMM					0x0502		// Stop resistance measurement
#define ACK_STOP_RMM_OK				0x05020001	
#define ACK_STOP_RMM_ERR			0x0502FFFF

#define GET_RMM_DATA				0x0511

#define START_RMM_CALIB				0x0521
#define STOP_RMM_CALIB				0x0522

#define SAVE_SCAN_RMM_35K			0x0801
#define SAVE_SCAN_RMM_300K			0x0802
#define SAVE_STIM_RMM_10V			0x0803
#define SAVE_STIM_RMM_25V			0x0804

#define GET_SCAN_RMM_35K			0x0811
#define GET_SCAN_RMM_300K			0x0812
#define GET_STIM_RMM_10V			0x0813
#define GET_STIM_RMM_25V			0x0814

// Device Calibration
#define CALIB_UPDATE_DAC			0x0901

#define SAVE_25UA_VALUE				0x0910
#define SAVE_500UA_VALUE			0x0911
#define SAVE_1000UA_VALUE			0x0912
#define SAVE_1500UA_VALUE			0x0913
#define SAVE_2000UA_VALUE			0x0914
#define SAVE_2500UA_VALUE			0x0915

#define UPDATE_DAC_25UA				0x0920
#define UPDATE_DAC_500UA			0x0921
#define UPDATE_DAC_1000UA			0x0922
#define UPDATE_DAC_1500UA			0x0923
#define UPDATE_DAC_2000UA			0x0924
#define UPDATE_DAC_2500UA			0x0925

#define START_STIMULATION			0xAA01
#define ABORT_STIMULATION			0xAA10
#define DEVICE_ERR					0xFFFFFFFF

/* Functions */
uint32_t process_command(uint16_t command, uint16_t data);

#endif /* COMMANDS_H_ */