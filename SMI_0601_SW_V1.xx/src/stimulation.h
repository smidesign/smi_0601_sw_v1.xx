/*
 * stimulation.h
 *
 * Created: 5/30/2020 02:20:23 PM
 *  Author: mdev_soterix
 */ 


#ifndef STIMULATION_H_
#define STIMULATION_H_

#define RAMP_DURATION	31
#define SHAM_OFF		0
#define SHAM_ON			1


/* Describes current state of device */
enum _device_status{
	IDLE,				// Device is idle, waiting for command
	PRE_STIMULATION,	// scan mode
	RMM_CALIBRATION,	// Device is in resistance calibration mode	
	STIMULATION,		// Device is stimulating
	POST_STIMULATION,	// The stimulation has ended
	CURRENT_DATA_UPDATE	// Updating output current data
	};

/* Describes current state of stimulation */
enum _stim_status{
	RAMP_UP,			// Ramping up
	SHAM_RAMP_UP,		// Sham ramp up
	ACTIVE_STIM,		// Stimulating at intended current
	SHAM_STIM,			// Device at starting current
	SHAM_RAMP_DOWN,		// Sham ramp down
	RAMP_DOWN,			// Ramping down
	END_OF_STIM			// Stimulation ended
};

void stimulation(void);

#endif /* STIMULATION_H_ */