/*
 * eemem_address.h
 *
 * Created: 5/18/2020 06:26:00 PM
 *  Author: mdev_soterix
 */ 


#ifndef EEMEM_ADDRESS_H_
#define EEMEM_ADDRESS_H_

//All variables are 4 byte long

// Device specific

#define EEMEM_DEVICE_TYPE				0	// <<<< This is index not an actual address
#define EEMEM_DEVICE_SERIAL_NUMBER		1
// Intensity Settings
#define EEMEM_25UA						2
#define EEMEM_500UA						3
#define EEMEM_1000UA					4
#define EEMEM_1500UA					5
#define EEMEM_2000UA					6
#define EEMEM_2500UA					7

// Programmed Settings
#define EEMEM_CURRENT_INTENSITY			8
#define EEMEM_CURRENT_DURATION			9
#define EEMEM_CURRENT_SHAM				10

// RMM Data
#define EEMEM_SCAN_RMM_35K				11
#define EEMEM_SCAN_RMM_300K				12
#define EEMEM_STIM_RMM_10V				13
#define EEMEM_STIM_RMM_25V				14

#endif /* EEMEM_ADDRESS_H_ */