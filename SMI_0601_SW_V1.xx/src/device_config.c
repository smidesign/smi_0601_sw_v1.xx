/*
 * device_config.c
 *
 * Created: 5/18/2020 03:48:36 PM
 *  Author: mdev_soterix
 */ 

#include "main.h"
#include "device_config.h"
#include "led.h"
#include "AT25M02.h"
#include "AD5683RBRMZ.h"
#include "push_button.h"
#include "trigger_input.h"

extern led red_led, green_led, blue_led;
extern trigger_in start_abort_trigger;
extern push_button pb1, pb2;
extern Pdc *uart_pdc;
extern pdc_packet_t uart_pdc_packet;
extern uint8_t uart_pdc_buffer[UART_BUFFER_SIZE] ;
/**
 * \brief Initializes the device
 */
void init_device(void){
	sysclk_init();
	supc_switch_sclk_to_32kxtal(SUPC, 0);
	uint32_t osc_setup_time = 0;
	const uint32_t osc_setup_timeout = 120000000ul;
	while(osc_setup_time < osc_setup_timeout){
		if(pmc_osc_is_ready_32kxtal()){
			break;
		}
		osc_setup_time++;
	}
	sysclk_init();
	
	ioport_init();
	
	config_uart_serial();
	
	/* Initialize LEDs */
	init_leds();
	if(osc_setup_time >= osc_setup_timeout){
		for(uint32_t idx = 0; idx < 20; idx++){
			set_led_state(&red_led, LED_ON);
			delay_ms(25);
			set_led_state(&red_led, LED_OFF);
			delay_ms(100);
		}
	}
	
	init_push_buttons();
	
	// Initialize trigger
	start_abort_trigger.trigger_in_pin = TRIGGER_IN_PIN;
	start_abort_trigger.trigger_in_logic_level = TRIGGER_ACTIVE_HIGH;
	init_trigger(start_abort_trigger);
	
	
	/* Configure SPI and SPI devices */
	config_spi();
	
	init_rtt();
	init_tc0_0(RMM_FREQUENCY);
	init_tc0_1(TC0_1_FREQUENCY);
	init_tc0_2(TC0_2_FREQUENCY);
	
	tc_start(TC0, 0);
	tc_start(TC0, 1);
	tc_start(TC0, 2);
	
	init_adc();
	// Disable WDT
	WDT->WDT_MR = WDT_MR_WDDIS;
}

void init_leds(void){
	// Initialize LED
	red_led.led_pin = RED_LED_PIN;
	red_led.led_logic_level = LED_ACTIVE_LOW;
	red_led.led_state = LED_OFF;
	init_led(&red_led);
	
	green_led.led_pin = GREEN_LED_PIN;
	green_led.led_logic_level = LED_ACTIVE_LOW;
	green_led.led_state = LED_OFF;
	init_led(&green_led);
	
	blue_led.led_pin = BLUE_LED_PIN;
	blue_led.led_logic_level = LED_ACTIVE_LOW;
	blue_led.led_state = LED_OFF;
	init_led(&blue_led);
}

void init_push_buttons(void){
	pb1.pb_pin = PB1_PIN;
	pb1.pb_logic_level = PB_ACTIVE_HIGH;
	init_pb(pb1);
	
	pb2.pb_pin = PB2_PIN;
	pb2.pb_logic_level = PB_ACTIVE_HIGH;
	init_pb(pb2);
}

void config_uart_serial(void){
	// Initialize serial
	ioport_set_pin_mode(UART_RX_PIN, IOPORT_MODE_MUX_A);
	ioport_disable_pin(UART_RX_PIN);
	ioport_set_pin_mode(UART_TX_PIN, IOPORT_MODE_MUX_A);
	ioport_disable_pin(UART_TX_PIN);
	
	const usart_serial_options_t uart_serial_options = {
		.baudrate = 115200,
		.charlength = US_MR_CHRL_8_BIT,
		.paritytype = US_MR_PAR_NO,
		.stopbits = US_MR_NBSTOP_1_BIT
	};
	sysclk_enable_peripheral_clock(UART_ID);
	stdio_serial_init(UART_BASE, &uart_serial_options);
	
	uart_pdc = uart_get_pdc_base(UART_BASE);
	uart_pdc_packet.ul_addr = (uint32_t)uart_pdc_buffer;
	uart_pdc_packet.ul_size = UART_BUFFER_SIZE;
	
	pdc_rx_init(uart_pdc, &uart_pdc_packet, NULL);
	pdc_enable_transfer(uart_pdc, PERIPH_PTCR_RXTEN);// | PERIPH_PTCR_TXTEN);
	uart_enable_interrupt(UART_BASE, UART_IER_RXBUFF);
	NVIC_EnableIRQ(UART_IRQn);
}

void config_spi(void){
	/* Configure SPI and SPI devices here */
	// Initialize SPI pins
	ioport_set_pin_mode(SPI_MOSI_PIN, SPI_MOSI_PIN_FLAG);
	ioport_disable_pin(SPI_MOSI_PIN);
	
	ioport_set_pin_mode(SPI_MISO_PIN, SPI_MISO_PIN_FLAG);
	ioport_disable_pin(SPI_MISO_PIN);
	
	ioport_set_pin_mode(SPI_SPCK_PIN, SPI_SPCK_PIN_FLAG);
	ioport_disable_pin(SPI_SPCK_PIN);
	
	ioport_set_pin_mode(SPI_AD5683_NSS_PIN, SPI_AD5683_NSS_PIN_FLAG);
	ioport_disable_pin(SPI_AD5683_NSS_PIN);
	
	ioport_set_pin_mode(SPI_AT25M02_NSS_PIN, SPI_AT25M02_NSS_PIN_FLAG);
	ioport_disable_pin(SPI_AT25M02_NSS_PIN);
	
	spi_master_init(SPI_MASTER_BASE);
	
	init_AD5683();
	init_AT25M02();
	
	spi_enable(SPI_MASTER_BASE);
}

void init_tc0_0(uint32_t freq){
	uint32_t divisor = 0;
	uint32_t tc_clock = 0;
	uint32_t sysclk = sysclk_get_cpu_hz();
	pmc_enable_periph_clk(ID_TC0);
	
	tc_find_mck_divisor(freq, sysclk, &divisor, &tc_clock, sysclk);
	tc_init(TC0, 0, tc_clock | TC_CMR_CPCTRG);
	tc_write_rc(TC0, 0, ((sysclk / divisor) / freq));
	
	NVIC_DisableIRQ(TC0_IRQn);
	NVIC_ClearPendingIRQ(TC0_IRQn);
	NVIC_SetPriority(TC0_IRQn, 0);
	NVIC_EnableIRQ(TC0_IRQn);
	tc_enable_interrupt(TC0, 0, TC_IER_CPCS);
}

void init_tc0_1(uint32_t freq){
	uint32_t divisor = 0;
	uint32_t tc_clock = 0;
	uint32_t sysclk = sysclk_get_cpu_hz();
	pmc_enable_periph_clk(ID_TC1);
	
	tc_find_mck_divisor(freq, sysclk, &divisor, &tc_clock, sysclk);
	tc_init(TC0, 1, tc_clock | TC_CMR_CPCTRG);
	tc_write_rc(TC0, 1, ((sysclk / divisor) / freq));
	
	NVIC_DisableIRQ(TC1_IRQn);
	NVIC_ClearPendingIRQ(TC1_IRQn);
	NVIC_SetPriority(TC1_IRQn, 0);
	NVIC_EnableIRQ(TC1_IRQn);
	tc_enable_interrupt(TC0, 1, TC_IER_CPCS);
}

void init_tc0_2(uint32_t freq){
	uint32_t divisor = 0;
	uint32_t tc_clock = 0;
	uint32_t sysclk = sysclk_get_cpu_hz();
	pmc_enable_periph_clk(ID_TC2);
	
	tc_find_mck_divisor(freq, sysclk, &divisor, &tc_clock, sysclk);
	tc_init(TC0, 2, tc_clock | TC_CMR_CPCTRG);
	tc_write_rc(TC0, 2, ((sysclk / divisor) / freq));
	
	NVIC_DisableIRQ(TC2_IRQn);
	NVIC_ClearPendingIRQ(TC2_IRQn);
	NVIC_SetPriority(TC2_IRQn, 0);
	NVIC_EnableIRQ(TC2_IRQn);
	tc_enable_interrupt(TC0, 2, TC_IER_CPCS);
}

void init_adc(void){
	pmc_enable_periph_clk(ID_ADC);
	adc_init(ADC, sysclk_get_cpu_hz(), ADC_CLOCK_FREQUENCY, ADC_STARTUP_TIME_4);
	adc_configure_timing(ADC, 1, ADC_SETTLING_TIME_3, 1);
	adc_enable_tag(ADC);
	adc_stop_sequencer(ADC);
	
	/* Enable channels*/
	adc_enable_channel(ADC, SHUNT_ADC_CHANNEL);
	adc_enable_channel(ADC, LOAD_ADC_CHANNEL);
	
	/* Disable analog change */
	adc_disable_anch(ADC);
	
	/* Set ADC gain = 1 */
	adc_set_channel_input_gain(ADC, SHUNT_ADC_CHANNEL, ADC_GAINVALUE_0);
	adc_set_channel_input_gain(ADC, LOAD_ADC_CHANNEL, ADC_GAINVALUE_0);
	
	/* Disable input offset */
	adc_disable_channel_input_offset(ADC, SHUNT_ADC_CHANNEL);
	adc_disable_channel_input_offset(ADC, LOAD_ADC_CHANNEL);
	
	/* Set auto-calibration mode */
	adc_set_calibmode(ADC);
	while(1){
		if((adc_get_status(ADC) & ADC_ISR_EOCAL) == ADC_ISR_EOCAL){
			break;
		}
	}
	
	/* Disable power saving mode */
	adc_configure_power_save(ADC, 0, 0);
	
	/* Transfer without PDC */
	/* Enable Data ready interrupt. */
	adc_enable_interrupt(ADC, ADC_IER_DRDY);
	
	/* Enable ADC interrupt. */
	NVIC_EnableIRQ(ADC_IRQn);
	
	/* ADC software trigger */
	//adc_configure_trigger(ADC, ADC_TRIG_SW, 0);
	adc_configure_trigger(ADC, ADC_TRIG_SW, 1);
	
	adc_start(ADC);
}

/**
 * \brief Initialize RTT at 1Hz
 */
void init_rtt(void){
	rtt_sel_source(RTT, false);
	rtt_init(RTT, 32768);
	
	NVIC_DisableIRQ(RTT_IRQn);
	NVIC_ClearPendingIRQ(RTT_IRQn);
	NVIC_SetPriority(RTT_IRQn, 0);
	NVIC_EnableIRQ(RTT_IRQn);
	rtt_enable_interrupt(RTT, RTT_MR_RTTINCIEN);
}