/*
 * led.c
 *
 * Created: 5/12/2020 04:34:02 PM
 *  Author: mdev_soterix
 */ 

#include "led.h"

/**
 * \brief Initializes the led and sets a state
 * \param led structure
 */
void init_led(led *_led){
	// Initialize the pin
	ioport_set_pin_dir(_led->led_pin, IOPORT_DIR_OUTPUT);
	// Set default state based on logic level
	if(_led->led_logic_level == LED_ACTIVE_HIGH){
		ioport_set_pin_level(_led->led_pin, _led->led_state);
	}
	else if(_led->led_logic_level == LED_ACTIVE_LOW){
		ioport_set_pin_level(_led->led_pin, !(_led->led_state));
	}
	else{
		// error: shouldn't be here
	}
}

/*
 * \brief Sets led state
 * \param led structure, state of the led to be set
 */
void set_led_state(led *_led, enum _led_state led_state){
	if(_led->led_logic_level == LED_ACTIVE_HIGH){
		ioport_set_pin_level(_led->led_pin, led_state);
	}
	else if(_led->led_logic_level == LED_ACTIVE_LOW){
		ioport_set_pin_level(_led->led_pin, !(led_state));
	}
};

/*
 * \brief Toggle the led state
 * \param led structure
 */
void toggle_led_state(led *_led){
	_led->led_state = !(_led->led_state);
	
	if(_led->led_logic_level == LED_ACTIVE_HIGH){
		ioport_set_pin_level(_led->led_pin, _led->led_state);
	}
	else if(_led->led_logic_level == LED_ACTIVE_LOW){
		ioport_set_pin_level(_led->led_pin, !(_led->led_state));
	}
}

/*
 * \brief Get the state of led
 * \param led structure
 * \return The state of led
 */
enum _led_state get_led_state(led _led){
	return _led.led_state;
}